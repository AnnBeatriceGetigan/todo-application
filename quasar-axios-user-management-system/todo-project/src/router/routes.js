
const routes = [
  {
    // path: '/',
    // component: () => import('layouts/MainLayout.vue'),
    // children: [
    //   { path: '', component: () => import('pages/IndexPage.vue') }
    // ]
    path: "/",
    redirect: {
      name: "user-list",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "user-list",
        name: "user-list",
        component: () => import("pages/listUsers.vue"),
        props: true,
      },
      {
        path: "add-user/:id?",
        name: "add-user",
        component: () => import("pages/addUser.vue"),
      },
    ],

  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
