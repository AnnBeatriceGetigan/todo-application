import { ref } from "vue";
// Displays the data of the database in the table //
let columns = ref([
  {
    label: "ID",
    align: "left",
    field: "id",
    sortable: true,
  },
  {
    name: "name",
    label: "NAME",
    align: "left",
    field: "name",
    sortable: true,
  },
  {
    name: "username",
    label: "USERNAME",
    align: "left",
    field: "username",
    sortable: true,
  },
  {
    name: "email",
    label: "EMAIL",
    align: "left",
    field: "email",
    sortable: true,
  },
  {
    name: "phone",
    label: "Phone",
    align: "left",
    field: "phone",
    sortable: true,
  },
  //The format: (value) => Displays all the street, suite, city, zipcode //
  //The argument named "value" returns string that includes the address // 
  {
    name: "address",
    label: "ADDRESS",
    align: "left",
    field: "address",
    format: (value) =>
      `${value.street}, ${value.suite}, ${value.city}, ${value.zipcode}`,
  },
  {
    name: "website",
    label: "Website",
    align: "left",
    field: "website",
    sortable: true,
  },
  {
    name: "company",
    label: "Company",
    align: "left",
    field: "company",
    sortable: true,
    format: (value) =>
      `${value.name}`,
  },
  {
    name: "company",
    label: "Catchphrase",
    align: "center",
    field: "company",
    sortable: true,
    format: (value) =>
      `${value.catchPhrase}`,
  },
  {
    name: "company",
    label: "Business Services",
    align: "center",
    field: "company",
    sortable: true,
    format: (value) =>
    `${value.bs}`,
  },
  {
    name: "action",
    label: "ACTION",
    align: "center",
    field: "action",
  },
]);

let resetForm = (User) => {
  User.value = {
    name: "",
    username: "",
    email: "",
    address: {
      street: "",
      suite: "",
      city: "",
      zipcode: "",
    },
    phone: "",
    website: "",
    company: {
      name: "",
      catchPhrase: "",
      bs: "",
    },
  };
};

let User = ref({
  name: "",
  username: "",
  email: "",
  address: {
    street: "",
    suite: "",
    city: "",
    zipcode: "",
  },
  phone: "",
  website: "",
  company: {
    name: "",
    catchPhrase: "",
    bs: "",
  },
});
export { columns, resetForm, User };