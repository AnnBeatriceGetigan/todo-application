import axios from "axios";
let url = "http://localhost:3000/users";

let list_user = async () => {
  try {
    const { data } = await axios.get(url);
    return data;
  } catch (error) {
    throw new Error(error);
  }
};

let post_user = async (user) => {
  try {
    const { data } = await axios.post(url, user);
    return data;
  } catch (error) {
    throw new Error(error);
  }
};

const getByID = async (id) => {
  try {
    const { data } = await axios.get(`${url}/${id}`);
    return data;
  } catch (error) {
    throw new Error(error);
  }
};

let update_user = async (user) => {
  try {
    const { data } = await axios.put(`${url}/${user.id}`, user);
    return data;
  } catch (error) {
    throw new Error(error);
  }
};

let remove_user = async (id) => {
  try {
    const { data } = await axios.delete(`${url}/${id}`);
    return data;
  } catch (error) {
    throw new Error(error);
  }
};

export { list_user, post_user, update_user, remove_user, getByID };