import { createStore } from "vuex";

export const store = createStore({
  state: {
    users: [],
  },
  mutations: {
    setUsers(state, users) {
      state.users = users;
    },
    addUser(state, user) {
      state.users.unshift(user);
    },
    deleteUser(state, user) {
      state.users = state.users.filter((u) => u.id !== user.id);
    },
  },
});